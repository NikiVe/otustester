<?php


namespace Test;


use Tester\TestFunc;

class CountingCharacter implements TestFunc
{

    public function run(string $string): string {
        return (string)strlen($string);
    }
}