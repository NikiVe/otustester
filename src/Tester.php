<?php

namespace Tester;

class Tester
{
    public static $in  = 'test.#.in';
    public static $out = 'test.#.out';

    public static function start(string $path, TestFunc $obj, int $start = 0, int $end = 0) {
        if ($end < 0 || $start < 0) {
            die();
        }

        for ($i = $start; $end === 0 ? true : $i <= $end ;$i++) {
            $in  = str_replace('#', $i, static::$in);
            $out = str_replace('#', $i, static::$out);

            $inPath  = $path.$in;
            $outPath = $path.$out;

            if (is_file($inPath) && is_file($outPath)) {
                $inContent = trim(file_get_contents($inPath));
                $outContent = trim(file_get_contents($outPath));
            } else {
                break;
            }

            if ($inContent !== false && $outContent !== false) {
                $sT = microtime(true);

                $result = $obj->run($inContent);

                echo "Test №$i status: " . ($result === $outContent ? 'true' : 'false') . ' ';
                echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;
            }
        }
    }
}