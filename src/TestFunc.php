<?php


namespace Tester;


interface TestFunc
{
    public function run(string $values) : string ;
}